var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
gulp.task('nodemon', function() {
  nodemon({
    script: 'index.js',
    ext: 'js'
  })
  .on('restart', function() {
    console.log('>> node restart');
  })
});