'use strict';

var userDao = require('../dao/userDao');

var fs = require('fs')

module.exports = {

	getUsers: function(){
		return userDao.getUsers();
	}
}