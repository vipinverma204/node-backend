'use strict';

var fs = require('fs')

module.exports = {

	getUsers: function(){
		const data = () => fs.readFileSync('./data/users.txt', { endoding: 'utf8'})
		var userList = data();
		return JSON.stringify(userList.toString().split("\r\n"));
	}
}