'use strict'

var express = require('express');
var router = express.Router();

// Require controller modules.
var userController = require('./userController');
/// User ROUTES ///

// GET User Controller APIs.
router.get('/users', userController.users);

module.exports = router;