'use strict'

var userService = require('../services/userService');

var fs = require('fs')

exports.users = function(req, res) {
	res.writeHead(200, {'Content-Type': 'application/json'});
	res.write(userService.getUsers());
	return res.end();
};