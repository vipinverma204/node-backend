let config
try {
  config = require('./web')
} catch (ex) {
  if (ex.code === 'MODULE_NOT_FOUND') {
    throw new Error('No config for process type: web')
  }

  throw ex
}

module.exports = config